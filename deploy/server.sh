# Pull code
cd /var/www/html/devops-linux-v2
git checkout main
git pull origin main

# Build and deploy
#yarn install
npm install --update-env
npm run build
pm2 restart server
