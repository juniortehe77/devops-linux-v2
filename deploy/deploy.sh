#!/bin/bash

DEPLOY_SERVER=192.168.1.7


# Building React output
npm init
npm install
npm run build

echo "Deploying to ${DEPLOY_SERVER}"
ssh vagrant@${DEPLOY_SERVER} 'bash' < ./deploy/server.sh

echo "Finished copying the build files"
